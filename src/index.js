import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Home, Category} from './components';
import { Router, Route, browserHistory, IndexRoute  } from 'react-router'
import './index.css';

ReactDOM.render((
    <Router history={browserHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={Home} />
            <Route path="/:category(/:id)" component={Category} />
        </Route>
    </Router>
), document.getElementById('root'));