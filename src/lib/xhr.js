export const baseURL = 'http://swapi.co/api';

const xhr = (baseURL) => (query) => fetch(baseURL + query).then(resp => resp.json());

export default xhr(baseURL);