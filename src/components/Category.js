import React from 'react';
import {List} from './List/List';
import {Info} from './Info/Info';
import {Search} from './List/Search';

import './Category.css'

export const Category = (props) => {
    return (
        <div className="Category">
            <div className="results">
                <h4>{props.params.category.toUpperCase()}</h4>
                <Search updateFilter={props.updateFilter} />
                <List
                    categoryData={props.categoryData}
                    filter={props.filter}
                    updateFilter={props.updateFilter} />
            </div>
            <Info
                categoryData={props.categoryData}
                currItem={props.currItem} />
        </div>
    )
};