import React from 'react';
import {Link} from 'react-router'
import './Nav.css'

export const NavItem = (props) => {
    const cat = props.category;
    const link = `/${cat}`;
    const label = cat.toUpperCase();

    return (
        <li>
            <Link to={link} activeClassName="active">{label}</Link>
        </li>
    )
};