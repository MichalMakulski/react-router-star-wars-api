import React from 'react';
import {Link} from 'react-router';
import {Nav} from './Nav';
import './Header.css';

export const Header = () => {
    return (
        <div className="Header">
            <h1>
                <Link to="/">Star Wars</Link>
            </h1>
            <Nav />
        </div>
    )
};