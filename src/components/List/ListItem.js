import React from 'react';
import {Link} from 'react-router';
import {baseURL} from '../../lib/xhr';
import './ListItem.css'

export const ListItem = (props) => {
    return (
        <li className="ListItem">
            <Link to={props.url.replace(baseURL, '')}>{props.name}</Link>
        </li>
    )
};