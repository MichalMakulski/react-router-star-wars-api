import React from 'react';
import {PaginationBtn} from './PaginationBtn'
import {baseURL} from '../../lib/xhr';
import './Pagination.css'

export const Pagination = (props) => {
    const isPrev = props.categoryData.previous;
    const isNext = props.categoryData.next;

    return (
        <div className="Pagination">
            <PaginationBtn
                label="Previous"
                to={isPrev && props.categoryData.previous.replace(baseURL, '')}
                isDisabled={!isPrev} />
            <PaginationBtn
                label="Next"
                to={isNext && props.categoryData.next.replace(baseURL, '')}
                isDisabled={!isNext} />
        </div>
    )
};