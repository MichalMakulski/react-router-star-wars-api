import React from 'react';
import './Search.css'

export const Search = (props) => {
    return (
        <input
            type="text"
            className="Search"
            placeholder="Search..."
            onChange={e => props.updateFilter(e.target.value)} />
    )
};