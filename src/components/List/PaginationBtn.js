import React from 'react';
import {Link} from 'react-router';
import './PaginationBtn.css'

export const PaginationBtn = (props) => {
    if (props.isDisabled) {
        return <span className="PaginationBtn disabled">{props.label}</span>
    }

    return <Link to={props.to} className="PaginationBtn">{props.label}</Link>
};