import React from 'react';
import {ListItem} from './ListItem';
import {Pagination} from './Pagination';
import './List.css'

export const List = (props) => {
    if (!props.categoryData) {
        return <div className="List">
            <p>Loading...</p>
        </div>
    }

    const items = props.categoryData.results
        .filter(item => item.name.toLowerCase().indexOf(props.filter) !== -1)
        .map((item, idx) => <ListItem key={idx} {...item}/>);

    return (
        <div className="List">
            <Pagination categoryData={props.categoryData} />
            <ul>
                {items}
            </ul>
        </div>
    )
};