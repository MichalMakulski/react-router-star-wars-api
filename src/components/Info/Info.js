import React from 'react';
import './Info.css'

export const Info = (props) => {

    if (!props.categoryData || !props.currItem) {
        return <div className="Info">
            <p>Select item to display details</p>
        </div>
    }

    const item = props.categoryData.results.filter(item => item.url.indexOf(props.currItem) !== -1)[0];

    return (
        <div className="Info">
            {item.name}
        </div>
    )
};