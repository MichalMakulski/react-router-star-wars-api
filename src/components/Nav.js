import React from 'react';
import {NavItem} from './NavItem';

export const Nav = (props) => {

    return (
        <div className="main-nav">
            <ul>
                <NavItem category="people" />
                <NavItem category="planets" />
                <NavItem category="starships" />
            </ul>
        </div>
    )
};