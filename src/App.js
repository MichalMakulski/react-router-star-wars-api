import React, { Component } from 'react';
import {Header, Favs} from './components'
//import cache from './lib/cache';
import xhr from './lib/xhr';
import './App.css';

class App extends Component {
    updateFilter = (filter) => {
        this.setState({
            filter: filter.toLowerCase()
        })
    }

    getCategoryData = (category, query) => {

        if (category === undefined) {
            return null;
        }

        xhr(`/${category}/${query}`).then(data => {
            this.setState({
                categoryData: data
            });
        });
    }

    state = {
        categoryData: this.getCategoryData(this.props.params.category, this.props.location.search),
        currItem: this.props.params.id ? this.props.location.pathname : null,
        filter: ''
    };

    componentWillReceiveProps(nextProps) {
        const isNewCategory = nextProps.params.category !== this.props.params.category;
        const isNewItem = nextProps.params.id !== this.props.params.id;
        const areNewResults = nextProps.location.search !== this.props.location.search;
        const shouldUpdate = isNewCategory || areNewResults;

        if (shouldUpdate && !nextProps.params.id) {
            this.setState({
                categoryData: this.getCategoryData(nextProps.params.category, nextProps.location.search)
            });
        }

        if (isNewItem) {
            this.setState({
                currItem: nextProps.params.id ? nextProps.location.pathname : null
            });
        }
    }

    render() {
        const children = React.Children.map(this.props.children,
            (child) => React.cloneElement(child, {
                categoryData: this.state.categoryData,
                filter: this.state.filter,
                currItem: this.state.currItem,
                updateFilter: this.updateFilter
            })
        );
        return (
              <div className="App">
                  <Header />
                  {children}
                  <Favs />
              </div>
        );
    }
}

export default App;
